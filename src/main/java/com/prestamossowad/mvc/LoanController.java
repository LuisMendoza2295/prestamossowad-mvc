package com.prestamossowad.mvc;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.prestamossowad.aplicacion.generics.QueryFacade;
import com.prestamossowad.aplicacion.processlogic.PrestamosPL;
import com.prestamossowad.aplicacion.processlogic.SolicitudPrestamoPL;
import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Moneda;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

@Controller
@SessionAttributes("objLoggedUsuario")
public class LoanController {

	@RequestMapping(value = "/loanRequestRegister/{id}", method = RequestMethod.GET)
	public ModelAndView loanRequestRegister(@PathVariable("id") int idUsuario, Locale locale, Model model, HttpServletRequest request){
		try{
			HttpSession session = request.getSession();
			Usuario objUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
			Usuario objUsuarioPrestatario = idUsuario == 0 ? new Usuario() : QueryFacade.getInstance().getUsuario(idUsuario);
			
			TipoInteres[] lstTiposInteres = objUsuario.getLstTiposInteres();
			List<Moneda> lstMonedas = QueryFacade.getInstance().getAllMonedas();
			List<TasaInteres> lstTasasInteres = QueryFacade.getInstance().getAllTasasInteres(objUsuario);

			ModelAndView loanRequestRegisterModel = new ModelAndView("frmLoanRequest", "command", new SolicitudPrestamo());
			loanRequestRegisterModel.addObject("objUsuarioPrestatario", objUsuarioPrestatario);
			loanRequestRegisterModel.addObject("lstTasasInteres", lstTasasInteres);
			loanRequestRegisterModel.addObject("lstMonedas", lstMonedas);
			
			session.setAttribute("objUsuarioPrestatario", objUsuarioPrestatario);
			
			return loanRequestRegisterModel;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("frmLoanRequest", "command", new SolicitudPrestamo());
	}
	
	@RequestMapping(value = "/loanRequestRegister/saveLoanRequest", method = RequestMethod.POST)
	public ModelAndView saveLoanRequest(@Validated SolicitudPrestamo objSolicitudPrestamo, Model model, HttpServletRequest request){
		
		ModelAndView loanRequestRegisterModel = new ModelAndView("frmLoanRequest", "command", new TipoInteres());
		
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
		
		try{
			if(request.getParameter("btnSave") != null){				
				if(objSolicitudPrestamo.getId() == 0){			
					int idTipoInteres = Integer.parseInt(request.getParameter("cmbInterestType").toString());
					int idMoneda = Integer.parseInt(request.getParameter("cmbCurrency").toString());
					String result = SolicitudPrestamoPL.getInstance().registrarSolicitudPrestamo(objSolicitudPrestamo, objLoggedUsuario, (Usuario) session.getAttribute("objUsuarioPrestatario"), idTipoInteres, idMoneda);
					
					loanRequestRegisterModel.addObject("message", result);
					
					session.setAttribute("objUsuarioPrestatario", null);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			loanRequestRegisterModel.addObject("message", e.getMessage());
		}
		
		TipoInteres[] lstTiposInteres = objLoggedUsuario.getLstTiposInteres();
		List<Moneda> lstMonedas = QueryFacade.getInstance().getAllMonedas();
		
		loanRequestRegisterModel.addObject("lstTiposInteres", lstTiposInteres);
		loanRequestRegisterModel.addObject("lstMonedas", lstMonedas);
		
		objLoggedUsuario.setLstTiposInteres(QueryFacade.getInstance().getAllTiposInteres(objLoggedUsuario));
		
		return loanRequestRegisterModel;
	}
	
	@RequestMapping(value = "/pendingApprovalLoans/{id}", method = RequestMethod.GET)
	public ModelAndView pendingApprovalLoans(@PathVariable("id") int idSolicitudPrestamo, Locale locale, Model model, HttpServletRequest request){
		try{
			HttpSession session = request.getSession();
			Usuario objUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
			
			SolicitudPrestamo objSelectedSolicitudPrestamo = idSolicitudPrestamo == 0 ? new SolicitudPrestamo() : QueryFacade.getInstance().getSolicitudPrestamo(idSolicitudPrestamo);					
			
			TipoInteres[] lstTiposInteres = idSolicitudPrestamo == 0 ? new TipoInteres[0] : objSelectedSolicitudPrestamo.getObjUsuarioPrestamista().getLstTiposInteres();
			List<Moneda> lstMonedas = QueryFacade.getInstance().getAllMonedas();
			List<TasaInteres> lstTasasInteres = QueryFacade.getInstance().getAllTasasInteres(objSelectedSolicitudPrestamo.getObjUsuarioPrestamista());
			
			List<SolicitudPrestamo> lstSolicitudesPrestamosAsPrestamista = QueryFacade.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(objUsuario);
			List<SolicitudPrestamo> lstSolicitudesPrestamosAsPrestatario = QueryFacade.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(objUsuario);

			ModelAndView loanRequestRegisterModel = new ModelAndView("frmPendingApprovalLoanRequests", "command", new SolicitudPrestamo());
			loanRequestRegisterModel.addObject("objUsuario", objUsuario);
			loanRequestRegisterModel.addObject("objSelectedSolicitudPrestamo", objSelectedSolicitudPrestamo);
			loanRequestRegisterModel.addObject("lstSolicitudesPrestamosAsPrestamista", lstSolicitudesPrestamosAsPrestamista);
			loanRequestRegisterModel.addObject("lstSolicitudesPrestamosAsPrestatario", lstSolicitudesPrestamosAsPrestatario);
			loanRequestRegisterModel.addObject("lstTasasInteres", lstTasasInteres);
			loanRequestRegisterModel.addObject("lstMonedas", lstMonedas);
			
			return loanRequestRegisterModel;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("frmPendingApprovalLoanRequests", "command", new SolicitudPrestamo());
	}
	
	@RequestMapping(value = "/pendingApprovalLoans/saveLoanRequestUpdate", method = RequestMethod.POST)
	public ModelAndView saveLoanRequestUpdate(@Validated SolicitudPrestamo objSolicitudPrestamo, Model model, HttpServletRequest request){		
		ModelAndView loanRequestRegisterModel = new ModelAndView("frmPendingApprovalLoanRequests", "command", new SolicitudPrestamo());
		
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");		
		
		int idTipoInteres = Integer.parseInt(request.getParameter("cmbInterestType").toString());
		int idMoneda = Integer.parseInt(request.getParameter("cmbCurrency").toString());
		
		try{
			if(request.getParameter("btnReply") != null){				
				if(objSolicitudPrestamo.getId() != 0){				
					objSolicitudPrestamo.setFechaModificacion(new Date());
					
					String message = SolicitudPrestamoPL.getInstance().reenviarSolicitud(objSolicitudPrestamo, objLoggedUsuario, idTipoInteres, idMoneda);
					
					loanRequestRegisterModel.addObject("message", message);
				}
			}
			
			if(request.getParameter("btnAccept") != null){
				if(objSolicitudPrestamo.getId() != 0){			
					objSolicitudPrestamo.setFechaModificacion(new Date());
					
					String message = SolicitudPrestamoPL.getInstance().aceptarSolicitud(objSolicitudPrestamo, objLoggedUsuario, idTipoInteres, idMoneda);
					
					loanRequestRegisterModel.addObject("message", message);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			loanRequestRegisterModel.addObject("message", e.getMessage());
		}
		
		TipoInteres[] lstTiposInteres = objSolicitudPrestamo.getObjUsuarioPrestamista().getLstTiposInteres();
		List<Moneda> lstMonedas = QueryFacade.getInstance().getAllMonedas();
		
		List<SolicitudPrestamo> lstSolicitudesPrestamosAsPrestamista = QueryFacade.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(objLoggedUsuario);
		List<SolicitudPrestamo> lstSolicitudesPrestamosAsPrestatario = QueryFacade.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(objLoggedUsuario);
		
		loanRequestRegisterModel.addObject("objSelectedSolicitudPrestamo", new SolicitudPrestamo());
		loanRequestRegisterModel.addObject("lstSolicitudesPrestamosAsPrestamista", lstSolicitudesPrestamosAsPrestamista);
		loanRequestRegisterModel.addObject("lstSolicitudesPrestamosAsPrestatario", lstSolicitudesPrestamosAsPrestatario);
		loanRequestRegisterModel.addObject("lstTiposInteres", lstTiposInteres);
		loanRequestRegisterModel.addObject("lstMonedas", lstMonedas);
		
		return loanRequestRegisterModel;
	}
	
	@RequestMapping(value = "/activeLoans", method = RequestMethod.GET)
	public ModelAndView activeLoans(Locale locale, Model model, HttpServletRequest request){
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
		
		List<Prestamo> lstPrestamosAsPrestamista = QueryFacade.getInstance().getAllPrestamosActivosAsPrestamista(objLoggedUsuario);
		List<Prestamo> lstPrestamosAsPrestatario = QueryFacade.getInstance().getAllPrestamosActivosAsPrestatario(objLoggedUsuario);
		
		ModelAndView activeLoansModel = new ModelAndView("frmActiveLoans", "command", new Prestamo());
		activeLoansModel.addObject("lstPrestamosAsPrestamista", lstPrestamosAsPrestamista);
		activeLoansModel.addObject("lstPrestamosAsPrestatario", lstPrestamosAsPrestatario);
		return activeLoansModel;
	}
	
	@RequestMapping(value = "/activeLoans/{id}", method = RequestMethod.GET)
	public ModelAndView selectActiveLoans(@PathVariable("id") int idPrestamo, Locale locale, Model model, HttpServletRequest request){
		try{
			HttpSession session = request.getSession();
			Usuario objUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
			
			Prestamo objPrestamo = QueryFacade.getInstance().getPrestamo(idPrestamo);				
			
			ModelAndView activeLoansRegisterModel = new ModelAndView("frmLoanFees", "command", new Cuota());
			activeLoansRegisterModel.addObject("objUsuario", objUsuario);
			activeLoansRegisterModel.addObject("objUsuarioPrestamista", objPrestamo.getObjSolicitudPrestamo().getObjUsuarioPrestamista());
			activeLoansRegisterModel.addObject("lstCuotas", objPrestamo.getLstCuotas());
			
			return activeLoansRegisterModel;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("frmLoanFees", "command", new Cuota());
	}
	
	@RequestMapping(value = "/payFee/{id}", method = RequestMethod.GET)
	public ModelAndView payFee(@PathVariable("id") int idCuota, Locale locale, Model model, HttpServletRequest request){
		try{
			if(idCuota != 0){
				HttpSession session = request.getSession();
				Usuario objUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
				
				Cuota objCuota = QueryFacade.getInstance().getCuota(idCuota);		
				
				ModelAndView activeLoansRegisterModel = new ModelAndView("frmLoanFees", "command", new Cuota());
				String message = "Cuota ya pagada";
				if(!objCuota.isPagado()){				
					message = PrestamosPL.getInstance().pagarCuota(objCuota);
					
					Prestamo objPrestamo = QueryFacade.getInstance().getPrestamo(objCuota);					
					
					activeLoansRegisterModel.addObject("objUsuario", objUsuario);
					activeLoansRegisterModel.addObject("objUsuarioPrestamista", objPrestamo.getObjSolicitudPrestamo().getObjUsuarioPrestamista());
					activeLoansRegisterModel.addObject("lstCuotas", objPrestamo.getLstCuotas());
				}
				activeLoansRegisterModel.addObject("message", message);
				
				return activeLoansRegisterModel;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("frmLoanFees", "command", new Cuota());
	}
}
