package com.prestamossowad.mvc;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.prestamossowad.aplicacion.generics.InsertFacade;
import com.prestamossowad.aplicacion.generics.SecurityFacade;
import com.prestamossowad.dominio.Usuario;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(Locale locale, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("objLoggedUsuario", new Usuario());
		
		return new ModelAndView("home", "command", new Usuario());
	}
	
	@RequestMapping(value = "/verifyAccess", method = RequestMethod.POST)
	public ModelAndView verifyAccess(@ModelAttribute("SpringWeb") Usuario objUsuario, ModelMap model, HttpServletRequest request){
		try{
			if(request.getParameter("btnLogin") != null){
				Usuario objUsuarioResult = SecurityFacade.getInstance().verifyAccess(objUsuario.getUsername(), objUsuario.getPassword());
				if(objUsuarioResult != null && objUsuarioResult.getId() != 0){
					HttpSession session = request.getSession();
					session.setAttribute("objLoggedUsuario", objUsuarioResult);
					
					ModelAndView modelAndView = new ModelAndView("main");
					modelAndView.addObject("objUsuario", objUsuarioResult);
	
					return modelAndView;
				}
			}
			if(request.getParameter("btnRegister") != null){
				ModelAndView modelAndView = new ModelAndView("frmUserRegister", "command", new Usuario());

				return modelAndView;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		ModelAndView errorModel = new ModelAndView("home", "command", new Usuario());
		errorModel.addObject("message", "Usuario o Password Incorrectos!");
		
		return errorModel;
	}
	
	@RequestMapping(value = "/userRegister", method = RequestMethod.POST)
	public ModelAndView userRegister(@ModelAttribute("SpringWeb") Usuario objUsuario, ModelMap model, HttpServletRequest request){
		String message = "";
		try{
			message = InsertFacade.getInstance().insertUsuario(objUsuario);
			if(objUsuario.getId() != 0){
				HttpSession session = request.getSession();
				session.setAttribute("objLoggedUsuario", objUsuario);
				
				ModelAndView modelAndView = new ModelAndView("main");
				modelAndView.addObject("objUsuario", objUsuario);

				return modelAndView;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		ModelAndView errorModel = new ModelAndView("frmUserRegister", "command", new Usuario());
		errorModel.addObject("message", message);
		
		return errorModel;
	}
}
