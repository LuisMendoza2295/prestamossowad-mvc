package com.prestamossowad.mvc;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.prestamossowad.aplicacion.generics.DeleteFacade;
import com.prestamossowad.aplicacion.generics.InsertFacade;
import com.prestamossowad.aplicacion.generics.QueryFacade;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.Usuario;

@Controller
@SessionAttributes("objLoggedUsuario")
public class InterestTypeController {

	@RequestMapping(value = "/interestTypeRegister/{id}", method = RequestMethod.GET)
	public ModelAndView interestTypeRegister(@PathVariable("id") int idTipoInteres, Locale locale, Model model, HttpServletRequest request){
		try{
			HttpSession session = request.getSession();
			Usuario objUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
			TipoInteres objSelectedTipoInteres = idTipoInteres == 0 ? new TipoInteres() : objUsuario.getTipoInteres(idTipoInteres);
			
			TasaInteres objSelectedTasaInteres = idTipoInteres == 0 ? new TasaInteres() : QueryFacade.getInstance().getTasaInteres(objSelectedTipoInteres);

			//TipoInteres[] arrayTiposInteres = objUsuario.getLstTiposInteres();
			
			ModelAndView interestTypeRegisterModel = new ModelAndView("frmInterestTypes", "command", new TipoInteres());
			//interestTypeRegisterModel.addObject("lstTiposInteres", arrayTiposInteres);
			List<TasaInteres> lstTasasInteres = QueryFacade.getInstance().getAllTasasInteres(objUsuario);
			interestTypeRegisterModel.addObject("lstTasasInteres", lstTasasInteres);
			interestTypeRegisterModel.addObject("objSelectedTipoInteres", objSelectedTipoInteres);
			interestTypeRegisterModel.addObject("objSelectedTasaInteres", objSelectedTasaInteres);
			
			return interestTypeRegisterModel;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return new ModelAndView("frmInterestTypes", "command", new TipoInteres());
	}
	
	@RequestMapping(value = "/interestTypeRegister/saveInterestType", method = RequestMethod.POST)
	public ModelAndView saveInterestType(@Validated TipoInteres objTipoInteres, Model model, HttpServletRequest request){
		
		ModelAndView interestTypeRegisterModel = new ModelAndView("frmInterestTypes", "command", new TipoInteres());
		
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
		
		try{
			if(request.getParameter("btnSave") != null){				
				if(objTipoInteres.getId() != 0){		
					TasaInteres objTasaInteres = new TasaInteres(
							0,
							//objTipoInteres.getObjTasaInteres().getTea(),
							new BigDecimal(request.getParameter("txtTea").toString()),
							//objTipoInteres.getObjTasaInteres().getTim(),
							new BigDecimal(request.getParameter("txtTim").toString()),
							new Date(),
							true,
							objTipoInteres);
					
					boolean result = InsertFacade.getInstance().insertTasaInteres(objTipoInteres, objTasaInteres);
					
					if(result){
						interestTypeRegisterModel.addObject("message", "Tipo de Interes actualizado correctamente");
					}else{
						interestTypeRegisterModel.addObject("message", "Error en la actualizacion");
					}
				}else{
					objTipoInteres.setFechaRegistro(new Date());
					
					//TasaInteres objTasaInteres = QueryFacade.getInstance().getTasaInteres(objTipoInteres);
					TasaInteres objTasaInteres = new TasaInteres(
							0,
							//objTipoInteres.getObjTasaInteres().getTea(),
							new BigDecimal(request.getParameter("txtTea").toString()),
							//objTipoInteres.getObjTasaInteres().getTim(),
							new BigDecimal(request.getParameter("txtTim").toString()),
							new Date(),
							true,
							objTipoInteres);
					
					boolean result = InsertFacade.getInstance().insertTipoInteres(objTasaInteres, objLoggedUsuario);
					
					if(result){
						interestTypeRegisterModel.addObject("message", "Tipo de Interes registrado correctamente");
					}else{
						interestTypeRegisterModel.addObject("message", "Error en el registro");
					}
				}
			}
			
			if(request.getParameter("btnClear") != null){
				//objLoggedUsuario.setLstTiposInteres(QueryFacade.getInstance().getAllTiposInteres(objLoggedUsuario));
				List<TasaInteres> lstTasasInteres = QueryFacade.getInstance().getAllTasasInteres(objLoggedUsuario);
				interestTypeRegisterModel.addObject("lstTasasInteres", lstTasasInteres);
				return interestTypeRegisterModel;
			}
			
			if(request.getParameter("btnDelete") != null){				
				boolean result = DeleteFacade.getInstance().deleteTipoInteres(objTipoInteres);
				
				if(result){
					interestTypeRegisterModel.addObject("message", "Tipo de Interes eliminado correctamente");
				}else{
					interestTypeRegisterModel.addObject("message", "Error en la eliminacion");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			interestTypeRegisterModel.addObject("message", e.getMessage());
		}
		
		//objLoggedUsuario.setLstTiposInteres(QueryFacade.getInstance().getAllTiposInteres(objLoggedUsuario));
		List<TasaInteres> lstTasasInteres = QueryFacade.getInstance().getAllTasasInteres(objLoggedUsuario);
		interestTypeRegisterModel.addObject("lstTasasInteres", lstTasasInteres);
		
		return interestTypeRegisterModel;
	}
}
