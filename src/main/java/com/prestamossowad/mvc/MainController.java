package com.prestamossowad.mvc;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.prestamossowad.aplicacion.generics.QueryFacade;
import com.prestamossowad.dominio.Usuario;

@Controller
@SessionAttributes("objLoggedUsuario")
public class MainController {

	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public ModelAndView main(Locale locale, Model model, HttpServletRequest request){
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
		
		ModelAndView mainModel = new ModelAndView("main", "command", new Usuario());
		mainModel.addObject("objUsuario", objLoggedUsuario);
		return mainModel;
	}
	
	@RequestMapping(value = "/selectUser", method = RequestMethod.GET)
	public ModelAndView selectUser(Locale locale, Model model, HttpServletRequest request){
		HttpSession session = request.getSession();
		Usuario objLoggedUsuario = (Usuario) session.getAttribute("objLoggedUsuario");
		
		List<Usuario> lstUsuarios = QueryFacade.getInstance().getAllUsuarios(objLoggedUsuario);
		
		ModelAndView selectUserModel = new ModelAndView("frmSelectUser", "command", new Usuario());
		selectUserModel.addObject("lstUsuarios", lstUsuarios);
		return selectUserModel;
	}
}
