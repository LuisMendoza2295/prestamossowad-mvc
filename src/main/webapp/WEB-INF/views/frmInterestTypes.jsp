<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tipos de Interes</title>
</head>
<body>
	<br>
	<a href='<c:url value="/main"></c:url>'>Regresar a la Pagina Principal</a>
	<br>
	<h1>Tipo de Interes seleccionado: ${objSelectedTipoInteres.nombre }</h1>
	<frm:form method="POST" action="saveInterestType">
		<table border="1">
			<tr style="display: none;">
				<td>ID:</td>
				<td><frm:input path="id" value="${objSelectedTipoInteres.id}"/></td>
			</tr>
			<tr>
				<td>Nombre:</td>
				<td><frm:input path="nombre" value="${objSelectedTipoInteres.nombre }"/></td>
			</tr>
			<tr>
				<td>Dias Plazos:</td>
				<td><frm:input path="diasPlazos" value="${objSelectedTipoInteres.diasPlazos }"/></td>
			</tr>
			<tr>
				<td>TEA:</td>
				<td><input type="text" name="txtTea" value="${objSelectedTasaInteres.tea }"/></td>
			</tr>
			<tr>
				<td>TIM:</td>
				<td><input type="text" name="txtTim" value="${objSelectedTasaInteres.tim }"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="btnSave" value="Save"/>
					<input type="submit" name="btnDelete" value="Delete" />
					<input type="submit" name="btnClear" value="Clear" />
				</td>
			</tr>
		</table>
	</frm:form>
	<br>
	<p>${message }</p>
	<br>
	<table border="1" style="width: 40%" class="table table-hover">
		<thead style="background-color: yellow">
			<tr>
				<th>Nombre</th>
				<th>Dias Plazos</th>
				<th>TEA</th>
				<th>TIM</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${lstTasasInteres }" var="objTasaInteres">
				<tr>
					<td>${objTasaInteres.objTipoInteres.nombre }</td>
					<td>${objTasaInteres.objTipoInteres.diasPlazos }</td>
					<td>${objTasaInteres.tea }</td>
					<td>${objTasaInteres.tim }</td>
					<td><a href='<c:url value="/interestTypeRegister/${objTasaInteres.objTipoInteres.id }"></c:url>'>Editar</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>