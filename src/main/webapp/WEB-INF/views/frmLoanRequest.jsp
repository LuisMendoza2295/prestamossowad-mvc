<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Solicitud de Prestamo </title>
</head>
<body>
	<br>
	<a href='<c:url value="/main"></c:url>'>Regresar a la Pagina Principal</a>
	<br>
	<c:choose>
		<c:when test="${fn:length(lstTasasInteres) gt 0}">
			<h1>Usuario Prestatario: ${objUsuarioPrestatario.nombre } ${objUsuarioPrestatario.apellidoPaterno } </h1>
			<frm:form method="POST" action="saveLoanRequest">
				<table border="1">
					<tr style="display: none;">
						<td>ID:</td>
						<td><frm:input path="id" value="${objSolicitudPrestamo.id}"/></td>
					</tr>
					<tr>
						<td>Monto:</td>
						<td><frm:input path="monto" value="${objSolicitudPrestamo.monto }"/></td>
					</tr>
					<tr>
						<td>Cuotas:</td>
						<td><frm:input path="cuotas" value="${objSolicitudPrestamo.cuotas }"/></td>
					</tr>
					<tr>
						<td>Tasa de Interes</td>
						<td>
							<select name="cmbInterestType">
								<c:forEach items="${lstTasasInteres }" var="objTasaInteres">
									<option value="${objTasaInteres.objTipoInteres.id }" label="${objTasaInteres.objTipoInteres.nombre } / TEA: ${objTipoInteres.objTasaInteres.tea}"></option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td>Moneda</td>
						<td>
							<select name="cmbCurrency">
								<c:forEach items="${lstMonedas }" var="objMoneda">
									<option value="${objMoneda.id }" label="${objMoneda.nombre }"></option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" name="btnSave" value="Save"/>
							<input type="reset" name="btnClear" value="Clear" />
						</td>
					</tr>
				</table>
			</frm:form>
		</c:when>
		<c:otherwise>
			<h2>No cuenta con Tipos de Interes Registrados</h2>
		</c:otherwise>
	</c:choose>
	<br>
	<p>${message }</p>
	<br>
</body>
</html>