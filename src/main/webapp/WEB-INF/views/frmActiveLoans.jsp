<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Prestamos Activos</title>
</head>
<body>
	<br>
	<br>
	<a href='<c:url value="/main"></c:url>'>Regresar a la Pagina Principal</a>
	<br>
	<br>
	<div>
		<div style="float:left; margin-right:30px; width:40%;">
			Como Prestamista:
			<table border="1" style="width: 100%" class="table table-hover">
				<thead style="background-color: yellow">
					<tr>
						<th>Prestatario</th>
						<th>Monto</th>
						<th>Cuotas</th>
						<th>Tasa de Interes</th>
						<th>Moneda</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lstPrestamosAsPrestamista }" var="objPrestamoAsPrestamista">
						<tr>
							<td>${objPrestamoAsPrestamista.objSolicitudPrestamo.objUsuarioPrestatario.nombre } ${objPrestamoAsPrestamista.objSolicitudPrestamo.objUsuarioPrestatario.apellidoPaterno }</td>
							<td>${objPrestamoAsPrestamista.objSolicitudPrestamo.monto }</td>
							<td>${objPrestamoAsPrestamista.objSolicitudPrestamo.cuotas }</td>
							<td>${objPrestamoAsPrestamista.objSolicitudPrestamo.objTasaInteres.objTipoInteres.nombre } / TEA: ${objPrestamoAsPrestamista.objSolicitudPrestamo.objTasaInteres.tea }</td>
							<td>${objPrestamoAsPrestamista.objSolicitudPrestamo.objMoneda.abreviado }</td>
							<td><a href='<c:url value="/activeLoans/${objPrestamoAsPrestamista.id }"></c:url>'>Ver Cuotas</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>	
		<div style="float:left; width:40%;">
			Como Prestatario
			<table border="1" style="width: 100%" class="table table-hover">
				<thead style="background-color: yellow">
					<tr>
						<th>Prestamista</th>
						<th>Monto</th>
						<th>Cuotas</th>
						<th>Tasa de Interes</th>
						<th>Moneda</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lstPrestamosAsPrestatario }" var="objPrestamoAsPrestatario">
						<tr>
							<td>${objPrestamoAsPrestatario.objSolicitudPrestamo.objUsuarioPrestamista.nombre } ${objPrestamoAsPrestatario.objSolicitudPrestamo.objUsuarioPrestamista.apellidoPaterno }</td>
							<td>${objPrestamoAsPrestatario.objSolicitudPrestamo.monto }</td>
							<td>${objPrestamoAsPrestatario.objSolicitudPrestamo.cuotas }</td>
							<td>${objPrestamoAsPrestatario.objSolicitudPrestamo.objTasaInteres.objTipoInteres.nombre } / TEA: ${objPrestamoAsPrestatario.objSolicitudPrestamo.objTasaInteres.tea }</td>
							<td>${objPrestamoAsPrestatario.objSolicitudPrestamo.objMoneda.abreviado }</td>
							<td><a href='<c:url value="/activeLoans/${objPrestamoAsPrestatario.id }"></c:url>'>Pagar Cuota</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>	
	</div>
</body>
</html>