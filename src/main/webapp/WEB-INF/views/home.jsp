<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>
	<div align="center">
		<frm:form method="POST" action="verifyAccess" >
			<table> 
				<tr>
					<td><frm:label path="username">Usuario:</frm:label></td>
					<td><frm:input path="username"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="password">Password:</frm:label></td>
					<td><frm:password path="password"></frm:password></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="btnLogin" value="Ingresar"/>
						<input type="submit" name="btnRegister" value="Registrate"/>
						<input type="reset" value="Limpiar"/>
					</td>
				</tr>			
			</table>
		</frm:form>
	</div>	
	<br>
	<div align="center"> ${message}</div>
</body>
</html>
