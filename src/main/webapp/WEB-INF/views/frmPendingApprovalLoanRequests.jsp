<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Solicitudes Pendientes</title>
</head>
<body>
	<br>
	<a href='<c:url value="/main"></c:url>'>Regresar a la Pagina Principal</a>
	<br>
	<c:choose>
		<c:when test="${objSelectedSolicitudPrestamo.id != 0 }">
			<h1>Solicitud seleccionada: ${objSelectedSolicitudPrestamo.objUsuarioPrestamista.nombre } -> ${objSelectedSolicitudPrestamo.objUsuarioPrestatario.nombre }</h1>
		</c:when>
		<c:otherwise>
			<h1>Solicitudes</h1>
		</c:otherwise>
	</c:choose>
	<frm:form method="POST" action="saveLoanRequestUpdate">
		<table border="1">
			<tr style="display: none;">
				<td>ID:</td>
				<td><frm:input path="id" value="${objSelectedSolicitudPrestamo.id}"/></td>
			</tr>
			<tr style="display: none;">
				<td>Aprobado Prestamista:</td>
				<td><frm:input path="aprobadoPrestamista" value="${objSelectedSolicitudPrestamo.aprobadoPrestamista}"/></td>
			</tr>
			<tr style="display: none;">
				<td>Aprobado Prestatario:</td>
				<td><frm:input path="aprobadoPrestatario" value="${objSelectedSolicitudPrestamo.aprobadoPrestatario}"/></td>
			</tr>
			<tr style="display: none;">
				<td>Prestamista:</td>
				<td><frm:input path="objUsuarioPrestamista.id" value="${objSelectedSolicitudPrestamo.objUsuarioPrestamista.id}"/></td>
			</tr>
			<tr style="display: none;">
				<td>Prestamista:</td>
				<td><frm:input path="objUsuarioPrestatario.id" value="${objSelectedSolicitudPrestamo.objUsuarioPrestatario.id}"/></td>
			</tr>
			<tr>
				<td>Monto:</td>
				<td><frm:input path="monto" value="${objSelectedSolicitudPrestamo.monto }"/></td>
			</tr>
			<tr>
				<td>Cuotas:</td>
				<td><frm:input path="cuotas" value="${objSelectedSolicitudPrestamo.cuotas }"/></td>
			</tr>
			<tr>
				<td>Tasa de Interes</td>
				<td>
					<select name="cmbInterestType">
						<c:forEach items="${lstTasasInteres }" var="objTasaInteres">
							<c:choose>
								<c:when test="${objTasaInteres.id == objSelectedSolicitudPrestamo.objTasaInteres.id }">
									<option value="${objTasaInteres.objTipoInteres.id }" label="${objTasaInteres.objTipoInteres.nombre } / TEA: ${objTasaInteres.tea}" selected="selected"></option>
								</c:when>
								<c:otherwise>
									<option value="${objTasaInteres.objTipoInteres.id }" label="${objTasaInteres.objTipoInteres.nombre } / TEA: ${objTasaInteres.tea}"></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td>Moneda</td>
				<td>
					<select name="cmbCurrency">
						<c:forEach items="${lstMonedas }" var="objMoneda">
							<c:choose>
								<c:when test="${objMoneda.id == objSelectedSolicitudPrestamo.objMoneda.id }">
									<option value="${objMoneda.id }" label="${objMoneda.nombre }" selected="selected"></option>
								</c:when>
								<c:otherwise>
									<option value="${objMoneda.id }" label="${objMoneda.nombre }"></option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="submit" name="btnReply" value="Reply"/>
					<input type="submit" name="btnAccept" value="Accept"/>
				</td>
			</tr>
		</table>
	</frm:form>
	<br>
	<p>${message }</p>
	<br>
	<div>
		<div style="float:left; margin-right:30px; width:40%;">
			Como Prestamista
			<table border="1" style="width: 100%" class="table table-hover">
				<thead style="background-color: yellow">
					<tr>
						<th>Prestatario</th>
						<th>Monto</th>
						<th>Cuotas</th>
						<th>Tasa de Interes</th>
						<th>Moneda</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lstSolicitudesPrestamosAsPrestamista }" var="objSolicitudPrestamoAsPrestamista">
						<tr>
							<td>${objSolicitudPrestamoAsPrestamista.objUsuarioPrestatario.nombre } ${objSolicitudPrestamoAsPrestamista.objUsuarioPrestatario.apellidoPaterno }</td>
							<td>${objSolicitudPrestamoAsPrestamista.monto }</td>
							<td>${objSolicitudPrestamoAsPrestamista.cuotas }</td>
							<td>${objSolicitudPrestamoAsPrestamista.objTasaInteres.objTipoInteres.nombre } / TEA: ${objSolicitudPrestamoAsPrestamista.objTasaInteres.tea }</td>
							<td>${objSolicitudPrestamoAsPrestamista.objMoneda.abreviado }</td>
							<td><a href='<c:url value="/pendingApprovalLoans/${objSolicitudPrestamoAsPrestamista.id }"></c:url>'>Actualizar</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>	
		<div style="float:left; width:40%;">
			Como Prestatario
			<table border="1" style="width: 100%" class="table table-hover">
				<thead style="background-color: yellow">
					<tr>
						<th>Prestamista</th>
						<th>Monto</th>
						<th>Cuotas</th>
						<th>Tasa de Interes</th>
						<th>Moneda</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${lstSolicitudesPrestamosAsPrestatario }" var="objSolicitudPrestamoAsPrestatario">
						<tr>
							<td>${objSolicitudPrestamoAsPrestatario.objUsuarioPrestamista.nombre } ${objSolicitudPrestamoAsPrestatario.objUsuarioPrestamista.apellidoPaterno }</td>
							<td>${objSolicitudPrestamoAsPrestatario.monto }</td>
							<td>${objSolicitudPrestamoAsPrestatario.cuotas }</td>
							<td>${objSolicitudPrestamoAsPrestatario.objTasaInteres.objTipoInteres.nombre } / TEA: ${objSolicitudPrestamoAsPrestatario.objTasaInteres.tea }</td>
							<td>${objSolicitudPrestamoAsPrestatario.objMoneda.abreviado }</td>
							<td><a href='<c:url value="/pendingApprovalLoans/${objSolicitudPrestamoAsPrestatario.id }"></c:url>'>Actualizar</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>	
	</div>
</body>
</html>