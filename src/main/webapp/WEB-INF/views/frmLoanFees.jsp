<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Prestamos Activos</title>
</head>
<body>
	<br>
	<br>
	<a href='<c:url value="/main"></c:url>'>Regresar a la Pagina Principal</a>
	<br>
	<br>
	<div>
		<table border="1" style="width: 100%" class="table table-hover">
			<thead style="background-color: yellow">
				<tr>
					<th>Monto</th>
					<th>Amortizacion</th>
					<th>Interes</th>
					<th>Mora</th>
					<th>Fecha Fin</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstCuotas }" var="objCuota">
					<tr>
						<td>${objCuota.monto }</td>
						<td>${objCuota.amortizacion }</td>
						<td>${objCuota.interes }</td>
						<td>${objCuota.mora }</td>
						<td>${objCuota.fechaFin }</td>
						<td>
							<c:if test="${objCuota.pagado == false && objUsuario.id != objUsuarioPrestamista.id}">
								<a href='<c:url value="/payFee/${objCuota.id }"></c:url>'>Pagar</a>
							</c:if>
							<c:if test="${objCuota.pagado == true }">
								Pagado
							</c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>	
	<br>
	<div align="center"> ${message}</div>
</body>
</html>