<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<br>
	<a href='<c:url value="/"></c:url>'>Regresar</a>
	<br>
	<div align="center">
		<frm:form method="POST" action="userRegister" >
			<table> 
				<tr>
					<td><frm:label path="nombre">Nombre:</frm:label></td>
					<td><frm:input path="nombre"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="apellidoPaterno">Apellido Paterno:</frm:label></td>
					<td><frm:input path="apellidoPaterno"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="apellidoMaterno">Apellido Materno:</frm:label></td>
					<td><frm:input path="apellidoMaterno"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="dni">DNI:</frm:label></td>
					<td><frm:input path="dni"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="username">Usuario:</frm:label></td>
					<td><frm:input path="username"></frm:input></td>
				</tr>
				<tr>
					<td><frm:label path="password">Password:</frm:label></td>
					<td><frm:password path="password"></frm:password></td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" name="btnSave" value="Registrar"/>
						<input type="reset" value="Limpiar"/>
					</td>
				</tr>			
			</table>
		</frm:form>
	</div>	
	<br>
	<div align="center"> ${message}</div>
</body>
</html>