<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.prestamossowad.dominio.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Principal</title>
</head>
<body>
	<h1>Principal</h1>
	<br>
	<a href='<c:url value="/"></c:url>'>Cerrar Sesion</a>
	<br>
	<h2>Hola ${objUsuario.nombre} ${objUsuario.apellidoPaterno}</h2>
	<br>
	<a href="${pageContext.request.contextPath}/interestTypeRegister/0">Tipos de Interes</a>
	<br>
	<a href="${pageContext.request.contextPath}/selectUser">Nueva Solicitud de Prestamo</a>
	<br>
	<a href="${pageContext.request.contextPath}/pendingApprovalLoans/0">Solicitudes Pendientes</a>
	<br>
	<a href="${pageContext.request.contextPath}/activeLoans">Prestamos Activos</a>
</body>
</html>