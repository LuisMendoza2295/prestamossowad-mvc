<%@ taglib uri="http://www.springframework.org/tags/form" prefix="frm" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Seleccionar Usuario</title>
</head>
<body>
	<br>
	<table border="1" style="width: 40%" class="table table-hover">
		<thead style="background-color: yellow">
			<tr>
				<th>Nombre</th>
				<th>Apellido Paterno</th>
				<th>Apellido Materno</th>
				<th>DNI</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${lstUsuarios }" var="objUsuario">
				<tr>
					<td>${objUsuario.nombre }</td>
					<td>${objUsuario.apellidoPaterno }</td>
					<td>${objUsuario.apellidoMaterno }</td>
					<td>${objUsuario.dni }</td>
					<td><a href='<c:url value="/loanRequestRegister/${objUsuario.id }"></c:url>'>Seleccionar</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>